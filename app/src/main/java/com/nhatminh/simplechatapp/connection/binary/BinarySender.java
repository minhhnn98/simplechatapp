package com.nhatminh.simplechatapp.connection.binary;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.nhatminh.simplechatapp.connection.base.MessageSender;
import com.nhatminh.simplechatapp.utils.BitmapUtils;
import com.nhatminh.simplechatapp.utils.FileUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_REQUEST_GET_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_REQUEST_NOT_GET_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_SEND_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_SEND_FILE_META_INFO;
import static com.nhatminh.simplechatapp.connection.ServerConnection.BINARY_MESSAGE_TYPE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.BUFFER_SIZE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.IMAGE_FILE_TYPE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.LOCAL_IMAGE_PATH;
import static com.nhatminh.simplechatapp.connection.ServerConnection.VIDEO_FILE_TYPE;

public class BinarySender extends MessageSender {

    protected OnFileSentListener fileSentCallback;
    protected OnBitmapDecodedListener bitmapDecodedCallback;

    public BinarySender(Socket socket, String id){
        super(socket, id);

        new Thread(new IdAndTypeSenderRunnable(BINARY_MESSAGE_TYPE)).start();
    }

    public void sendBinaryFile(String conversationId, String receiverId, String dataType, byte[] data, String hashValue) throws IOException {
        if(socket == null || receiverId == null || socketOutputStream == null){
            return;
        }

        socketOutputStream.writeUTF(ACTION_SEND_FILE);
        socketOutputStream.flush();

        socketOutputStream.writeUTF(conversationId);
        socketOutputStream.writeUTF(receiverId);
        socketOutputStream.writeUTF(dataType);
        socketOutputStream.writeUTF(hashValue);

        ArrayList<byte[]> chunks = divideByteArray(data, BUFFER_SIZE);
        socketOutputStream.writeInt(data.length);
        socketOutputStream.writeInt(chunks.size());
        socketOutputStream.flush();

        for(int i=0;i<chunks.size();i++){
            socketOutputStream.writeInt(chunks.get(i).length);
            socketOutputStream.write(chunks.get(i),0, chunks.get(i).length);
            socketOutputStream.flush();
        }

        fileSentCallback.onFileSentSucceed(conversationId, hashValue, data, dataType);

    }

    public void sendFileMetaInfo(String conversationId, String receiverId, String hashValue, String fileType, byte[] data) throws IOException{
        if(socket == null || receiverId == null || socketOutputStream == null){
            return;
        }

        socketOutputStream.writeUTF(ACTION_SEND_FILE_META_INFO);
        socketOutputStream.flush();

        socketOutputStream.writeUTF(conversationId);
        socketOutputStream.writeUTF(receiverId);
        socketOutputStream.writeUTF(fileType);
        socketOutputStream.flush();

        int checkSum = data.length;

        socketOutputStream.writeUTF(hashValue);
        socketOutputStream.writeInt(checkSum);
        socketOutputStream.flush();
    }

    public void decodeAndSendImageMetaInfo(String conversationId, String receiverId, String filePath) throws IOException {
        byte[] imageByte = decodeImage(filePath);

        String hashValue = FileUtils.hashWithMd5(imageByte);

        if(bitmapDecodedCallback != null){
            bitmapDecodedCallback.onDecodeCompleted(hashValue, filePath, imageByte);
        }

        sendFileMetaInfo(conversationId, receiverId, hashValue, IMAGE_FILE_TYPE, imageByte);
    }

    private byte[] decodeImage(String filePath){
        Bitmap bitmap = BitmapUtils.decodeBitmapWithRequestedSize(filePath, 1200, 1200);
        byte[] imageByte = BitmapUtils.bitmapToByteArray(bitmap);

        return imageByte;
    }

    public void sendVideoMetaInfo(String conversationId, String receiverId, String filePath) throws IOException {
        File file = new File(filePath);

        if(!file.exists()){
            return;
        }

        byte[] data = new byte[(int) file.length()];
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        DataInputStream dis = new DataInputStream(bis);
        dis.readFully(data);
        dis.close();

        String hashValue = FileUtils.hashWithMd5(data);

        sendFileMetaInfo(conversationId, receiverId, hashValue, VIDEO_FILE_TYPE, data);
    }

    public void sendFileRequested(String conversationId, String senderId,  String receiverId, String hashValue, String fileType, String action) throws IOException {
        if(socket == null || receiverId == null || socketOutputStream == null){
            return;
        }

        socketOutputStream.writeUTF(action);

        socketOutputStream.writeUTF(conversationId);
        socketOutputStream.writeUTF(senderId);
        socketOutputStream.writeUTF(receiverId);
        socketOutputStream.writeUTF(hashValue);
        socketOutputStream.writeUTF(fileType);
        socketOutputStream.flush();
    }

    private ArrayList<byte[]> divideByteArray(byte[] source, int chunkSize) {

        ArrayList<byte[]> result = new ArrayList<byte[]>();
        int start = 0;
        while (start < source.length) {
            int end = Math.min(source.length, start + chunkSize);
            result.add(Arrays.copyOfRange(source, start, end));
            start += chunkSize;
        }

        return result;
    }

    @Override
    public void disconnect(){
        super.disconnect();

        if(fileSentCallback != null){
            fileSentCallback = null;
        }

        if(bitmapDecodedCallback != null){
            bitmapDecodedCallback = null;
        }
    }


    public void setOnBitmapDecodedListener(OnBitmapDecodedListener listener){
        this.bitmapDecodedCallback = listener;
    }

    public void setOnFileSentListener(OnFileSentListener listener){
        this.fileSentCallback = listener;
    }

    public interface OnBitmapDecodedListener{
        void onDecodeCompleted(String hashValue, String filePath, byte[] data);
    }

    public interface OnFileSentListener{
        void onFileSentSucceed(String conversationId, String hashValue, byte[] data, String dataType);
    }
}
