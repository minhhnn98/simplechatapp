package com.nhatminh.simplechatapp.videoplayer;

public abstract class PlaybackInfoListener {

    public enum State{
        INVALID,
        PLAYING,
        PAUSED,
        RESET,
        COMPLETED
    }

    void onLogUpdated(String formattedMessage) {
    }

    void onDurationChanged(int duration) {
    }

    void onPositionChanged(int position) {
    }

    void onStateChanged(State state) {
    }

    void onPlaybackCompleted() {
    }
}
