package com.nhatminh.simplechatapp.imageloader;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Process;
import android.util.Log;
import android.util.LruCache;

import com.google.android.exoplayer2.offline.DownloadService;
import com.nhatminh.simplechatapp.utils.FileUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {

    private static final int MAX_THREAD = 5;

    private Context context;
    private boolean isRunning = false;

    private OnBitmapLoadingCompletedListener bitmapCompletedLoadingCallback;

    LruCache<String, Bitmap> memoryCache;
    ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREAD);

    public ImageLoader(Context context) {
        this.context = context;
        this.isRunning = true;

        createMemoryCache();
    }

    private void createMemoryCache(){
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory/8;

        memoryCache = new LruCache<String, Bitmap>(cacheSize){
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount()/1024;
            }
        };
    }

    public void cancelLoading(){
        isRunning = false;
    }

    public void clean(){
        if(bitmapCompletedLoadingCallback != null){
            bitmapCompletedLoadingCallback = null;
        }

        cancelLoading();
        clearMemoryCache();
        shutdownExecutor();
    }

    private void shutdownExecutor(){
        if(executorService != null){
            executorService.shutdown();
        }
    }

    private void clearMemoryCache(){
        if(memoryCache != null){
            memoryCache.evictAll();
        }
    }

    public void requestImage(String filePath, int position, int reqWidth, int reqHeight){

        RequestInfo requestInfo = new RequestInfo(position, reqHeight, reqWidth, filePath);

        // check on memory cache
        Bitmap bitmap = getBitmapFromMemoryCache(filePath);
        if(bitmap != null){
            bitmapCompletedLoadingCallback.onBitmapLoadingCompleted(bitmap, position);
            return;
        }

        // image not on cache, go on decoding it
        else {
            executorService.execute(new DecodeImageRunnable(requestInfo));
        }
    }

    private Bitmap getBitmapFromMemoryCache(String key){
        return memoryCache.get(key);
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap){
        if(getBitmapFromMemoryCache(key) == null && bitmap != null){
            memoryCache.put(key, bitmap);
        }
    }

    public void setOnBitmapLoadingCompletedListener(OnBitmapLoadingCompletedListener onBitmapLoadingCompletedListener){
        bitmapCompletedLoadingCallback = onBitmapLoadingCompletedListener;
    }


    public class DecodeImageRunnable implements Runnable{

        Bitmap bitmap;
        RequestInfo requestInfo;

        public DecodeImageRunnable(RequestInfo requestInfo){
            this.requestInfo = requestInfo;
            this.bitmap = null;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

            // check on disk
            if(FileUtils.isFileExisted(requestInfo.filePath)){
                bitmap = decodeBitmap(requestInfo);

                bitmapCompletedLoadingCallback.onBitmapLoadingCompleted(bitmap, requestInfo.requestId);

                addBitmapToMemoryCache(requestInfo.filePath, bitmap);
                return;
            }
        }
    }

    private Bitmap decodeBitmap(RequestInfo requestInfo){

        // get bitmap size without loading it to memory
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(requestInfo.filePath, options);

        // calculate sample size for scaling bitmap
        options.inSampleSize = calculateInSampleSize(options, requestInfo.reqWidth, requestInfo.reqHeight);

        // decode with sample size
        options.inJustDecodeBounds = false;

        Bitmap bitmap = null;

        try{
            bitmap = BitmapFactory.decodeFile(requestInfo.filePath, options);
        }
        catch (OutOfMemoryError ex){
            return null;
        }

        return isRunning?bitmap:null;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public interface OnBitmapLoadingCompletedListener {
        void onBitmapLoadingCompleted(Bitmap bitmap, int position);
    }

    private class RequestInfo{
        public int requestId;
        public int reqHeight;
        public int reqWidth;
        public String filePath;

        public RequestInfo(int requestId, int reqHeight, int reqWidth, String filePath) {
            this.requestId = requestId;
            this.reqHeight = reqHeight;
            this.reqWidth = reqWidth;
            this.filePath = filePath;
        }
    }
}
