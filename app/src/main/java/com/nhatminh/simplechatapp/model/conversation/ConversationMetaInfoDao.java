package com.nhatminh.simplechatapp.model.conversation;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ConversationMetaInfoDao {
    @Query("SELECT * FROM conversation_meta_info ORDER BY time_stamp DESC LIMIT (:n)")
    List<ConversationMetaInfo> getLastNConversations(int n);

    @Query("SELECT * FROM conversation_meta_info WHERE conversation_id = :conversationId")
    ConversationMetaInfo getConversationMetaInfo(String conversationId);

    @Update
    void updateMetaInfo(ConversationMetaInfo newMetaInfo);

    @Insert(onConflict = REPLACE)
    void insertMetaInfo(ConversationMetaInfo metaInfo);

    @Delete
    void deleteMetaInfo(ConversationMetaInfo metaInfo);
}
