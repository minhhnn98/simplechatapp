package com.nhatminh.simplechatapp.model.conversation;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.nhatminh.simplechatapp.model.message.UserMessage;

import java.util.List;

@Dao
public interface ConversationDao {

    @Insert
    void insert(Conversation... conversations);

    @Update
    void update(Conversation... conversations);

    @Delete
    void delete(Conversation... conversations);

    @Query("SELECT * FROM conversation WHERE conversation_id=:conversationId")
    Conversation getConversationWithId(String conversationId);
}
