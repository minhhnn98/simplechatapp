package com.nhatminh.simplechatapp.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateManipulation {

    static final String DATE_FORMAT = "dd/MM/yyyy";
    static SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    static SimpleDateFormat timestampFormat = new SimpleDateFormat("EEE, HH:mm");

    static {
        dateFormat.setLenient(false);
    }

    public static boolean isValidDate(String date){
        try {
            dateFormat.parse(date.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public static Date stringToDate(String dateString) {
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException | NullPointerException e) {
            return null;
        }
    }

    public static String dateToString(Date date) {
        return dateFormat.format(date);
    }

    public static SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public static String timestampToString(Timestamp timestamp){
        return timestampFormat.format(timestamp);
    }

    public static String longTimeToString(long time){
       return timestampToString(new Timestamp(time));
    }

}
