package com.nhatminh.simplechatapp.connection.textsocket;

import android.content.Context;

import com.nhatminh.simplechatapp.connection.base.MessageReceiver;
import com.nhatminh.simplechatapp.connection.base.SocketConnection;
import com.nhatminh.simplechatapp.model.message.UserMessage;

import java.io.IOException;

public class TextSocket extends SocketConnection {


    public TextSocket(String host, int port, String uId, Context context) throws IOException {
        super(host, port, uId, context);
        sender = new TextSender(socket, id);
        receiver = new TextReceiver(socket);

        receiver.setOnMessageReceivedListener(new MessageReceiver.OnMessageReceivedListener() {
            @Override
            public void onMessageReceived(String conversationId, String senderId, String message, long timestamp, String fileUrl) {
                onNewMessageAdded(createUserMessage(message, conversationId, senderId, timestamp, fileUrl));
            }
        });
    }

    public void sendTextMessage(String content, String conversationId, String receiverId) throws IOException {
        synchronized (sender) {

            ((TextSender) sender).sendTextMessage(content, conversationId, receiverId);

            UserMessage userMessage = createUserMessage(content, conversationId, id, System.currentTimeMillis(), null);
            onNewMessageAdded(userMessage);
        }
    }
}
