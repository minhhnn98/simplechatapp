package com.nhatminh.simplechatapp.conversation;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nhatminh.simplechatapp.R;
import com.nhatminh.simplechatapp.database.ConversationDatabase;
import com.nhatminh.simplechatapp.model.conversation.Conversation;
import com.nhatminh.simplechatapp.model.message.UserMessage;
import com.nhatminh.simplechatapp.utils.FileUtils;
import com.nhatminh.simplechatapp.utils.GlobalConstants;
import com.nhatminh.simplechatapp.connection.ServerConnection;
import com.nhatminh.simplechatapp.videoplayer.VideoPlayerActivity;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConversationActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int GALLERY_REQUEST_CODE = 100;
    public static final String FILE_PATH = "file_path";

    // conversation id and title from previous activity
    String conversationId;
    String conversationTitle;

    HashMap<String, String> members = new HashMap<>();
    List<UserMessage> messageList = new ArrayList<>();

    RecyclerView messageRecycler;
    MessageListAdapter messageAdapter;
    Button btSendText, btSendImage;
    EditText etMessageContent;
    LinearLayoutManager linearLayoutManager;

    FirebaseUser currentUser;

    Conversation conversation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        conversationId = getIntent().getExtras().getString(GlobalConstants.CONVERSATION_ID);
        conversationTitle = getIntent().getExtras().getString(GlobalConstants.CONVERSATION_TITLE);

        messageList = ConversationDatabase.getInstance(this).getUserMessageDao().getAllMessageOfConversation(conversationId);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        setupView();

        // get name from firebase later
        conversation = ConversationDatabase.getInstance(this).getConversationDao().getConversationWithId(conversationId);
        members = new HashMap<>();
        List<String> memberIds = conversation.getMembers();
        for (int i = 0; i < memberIds.size(); i++) {
            members.put(memberIds.get(0), "Default");
        }

        ServerConnection.getInstance(currentUser.getUid(), this).setActivity(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        createAndSetAdapter();
    }

    private void scrollToBottomMessageList() {
        int messageCount = messageAdapter.getItemCount();
        messageRecycler.smoothScrollToPosition(messageCount - 1);
    }

    private void createAndSetAdapter() {
        messageAdapter = new MessageListAdapter(this, messageList, currentUser.getUid(), members);
        messageRecycler.setLayoutManager(linearLayoutManager);
        messageRecycler.setAdapter(messageAdapter);
    }

    private void setupView() {
        messageRecycler = findViewById(R.id.reyclerview_message_list);
        btSendText = findViewById(R.id.btSendText);
        etMessageContent = findViewById(R.id.etMessageContent);

        btSendImage = findViewById(R.id.btSendImage);
        btSendImage.setOnClickListener(this);

        btSendText.setOnClickListener(this);

        btSendText.setEnabled(false);

        //disable send button when edit text is empty
        etMessageContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 0) {
                    btSendText.setEnabled(false);
                } else {
                    btSendText.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        linearLayoutManager = new LinearLayoutManager(this);

        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setSmoothScrollbarEnabled(true);

        messageRecycler.addOnItemTouchListener(new MessageListItemClickListener(this, messageRecycler, new MessageListItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String filePath = messageList.get(position).getFileUrl();

                if(filePath != null){
                    Intent intent = new Intent(ConversationActivity.this, VideoPlayerActivity.class);
                    intent.putExtra(FILE_PATH, filePath);
                    startActivity(intent);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etMessageContent.getWindowToken(), 0);
    }

    private String getLocalIpAddress() throws UnknownHostException {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipInt = wifiInfo.getIpAddress();
        return InetAddress.getByAddress(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ipInt).array()).getHostAddress();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btSendText:
                handleActionSendMessage();
                break;
            case R.id.btSendImage:
                chooseImageAndVideo();
                break;
        }
    }


    private void sendBinaryFileMetaInfo(final String filePath, final String fileType){
        new Thread(new Runnable() {
            @Override
            public void run() {
                //handle group chat later
                conversation.getMembers().remove(currentUser.getUid());

                try {
                    ServerConnection.getInstance(currentUser.getUid(), ConversationActivity.this)
                            .sendBinaryFileMetaInfo(conversationId, conversation.getMembers().get(0), fileType, filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void chooseImageAndVideo(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[] {"image/*", "video/*"});
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode){
                case GALLERY_REQUEST_CODE:
                    Uri selectedFile = data.getData();
                    String mimeType = getMimeType(selectedFile);
                    String filePath = FileUtils.getRealPath(this, selectedFile);

                    if(mimeType.contains("image")){
                        sendBinaryFileMetaInfo(filePath, ServerConnection.IMAGE_FILE_TYPE);
                    }
                    else if (mimeType.contains("video")) {
                        sendBinaryFileMetaInfo(filePath, ServerConnection.VIDEO_FILE_TYPE);
                    }

                    break;

                default:
                    super.onActivityResult(requestCode, resultCode, data);
                    break;
            }
    }

    public String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    private void handleActionSendMessage() {
        final String message = etMessageContent.getText().toString();
        new Thread(new Runnable() {
            @Override
            public void run() {
                //handle group chat later
                conversation.getMembers().remove(currentUser.getUid());

                try {
                    ServerConnection.getInstance(currentUser.getUid(), ConversationActivity.this)
                            .sendTextMessage(message, conversationId, conversation.getMembers().get(0));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            etMessageContent.getText().clear();
                            hideKeyboard();
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void insertToMessageList(UserMessage message) {
        messageList.add(message);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageAdapter.notifyItemInserted(messageList.size() - 1);
                scrollToBottomMessageList();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ServerConnection.getInstance(currentUser.getUid(), this).setActivity(null);
        messageAdapter.clean();
    }

    private void test() {
        Conversation conversation = new Conversation(conversationId);
        List<String> members = new ArrayList<>();
        members.add("N2rocWYboMQycyVAu5PhEarYS5n2");
        members.add("mnlJhvBohLNDWT2NiOp4NSp5VlG2");
        conversation.setMembers(members);

        ConversationDatabase.getInstance(this).getConversationDao().insert(conversation);


        UserMessage message1 = new UserMessage();
        message1.setConversationId(conversationId);
        message1.setCreatedAt(System.currentTimeMillis());
        message1.setMessageContent("Hi");
        message1.setSenderId("N2rocWYboMQycyVAu5PhEarYS5n2");
        message1.setFileUrl(null);

        UserMessage message2 = new UserMessage();
        message2.setConversationId(conversationId);
        message2.setCreatedAt(System.currentTimeMillis());
        message2.setMessageContent("Hello");
        message2.setSenderId("mnlJhvBohLNDWT2NiOp4NSp5VlG2");
        message2.setFileUrl(null);

        ConversationDatabase.getInstance(this).getUserMessageDao().insert(message1);
        ConversationDatabase.getInstance(this).getUserMessageDao().insert(message2);

       /* Conversation conversation = new Conversation(conversationId);
        List<String> members = new ArrayList<>();
        members.add("N2rocWYboMQycyVAu5PhEarYS5n2");
        members.add("mnlJhvBohLNDWT2NiOp4NSp5VlG2");
        conversation.setMembers(members);

        ConversationDatabase.getInstance(this).getConversationDao().insert(conversation);*/
    }
}
