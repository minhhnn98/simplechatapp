package com.nhatminh.simplechatapp.videoplayer;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlayerHolder implements PlayerAdapter {
    public static final int PLAYBACK_POSITION_REFRESH_INTERVAL_MS = 1000;
    public static final String LOG = "MediaPlayerHolder";

    private Context context;
    private VideoView videoView;
    private int resourceId;
    private PlaybackInfoListener playbackInfoListener;
   // private ScheduledExecutorService executor;
    //private Runnable seekbarPositionUpdateTask;
    private String videoPath;

    public VideoPlayerHolder(Context context, VideoView videoView) {
        this.context = context;
        this.videoView = videoView;
    }

    private void initializeVideoPlayer() {
        if (videoView != null) {
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopUpdatingCallbackWithPosition(true);

                    if (playbackInfoListener != null) {
                        playbackInfoListener.onStateChanged(PlaybackInfoListener.State.COMPLETED);
                        playbackInfoListener.onPlaybackCompleted();
                    }
                }
            });
        }
    }

    public void setPlaybackInfoListener(PlaybackInfoListener listener) {
        playbackInfoListener = listener;
    }


    @Override
    public void loadMedia(int resourceId) {
        this.resourceId = resourceId;

        String path = "android.resource://" + context.getPackageName() + "/" + resourceId;

        loadMedia(path);
    }

    @Override
    public void loadMedia(String filePath) {
        this.videoPath = filePath;

        initializeVideoPlayer();

        try {
            videoView.setVideoURI(Uri.parse(filePath));
        } catch (Exception e) {
            Log.e(LOG, "Set data source failed");
        }

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                initializeProgressCallback();
            }
        });
    }

    private void reloadMedia(){
        initializeVideoPlayer();

        try {
            videoView.setVideoURI(Uri.parse(this.videoPath));
        } catch (Exception e) {
            Log.e(LOG, "Set data source failed");
        }

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                initializeProgressCallback();
            }
        });
    }

    @Override
    public void release() {
        if(videoView != null){
            videoView.stopPlayback();
            videoView = null;
        }
    }

    @Override
    public boolean isPlaying() {
        if (videoView != null) {
            return videoView.isPlaying();
        }
        return false;
    }

    @Override
    public void play() {
        if (videoView != null && !videoView.isPlaying()) {
            videoView.start();

            if (playbackInfoListener != null) {
                playbackInfoListener.onStateChanged(PlaybackInfoListener.State.PLAYING);
            }

            updateProgressCallbackTask();
        }
    }

    @Override
    public void reset() {
        if (videoView != null) {
            videoView.stopPlayback();
            reloadMedia();
            videoView.seekTo(0);
            videoView.start();

            if (playbackInfoListener != null) {
                playbackInfoListener.onStateChanged(PlaybackInfoListener.State.RESET);
            }

            stopUpdatingCallbackWithPosition(true);
        }
    }

    @Override
    public void pause() {
        if (videoView != null && videoView.isPlaying()) {
            videoView.pause();

            if (playbackInfoListener != null) {
                playbackInfoListener.onStateChanged(PlaybackInfoListener.State.PAUSED);
            }

        }
    }

    @Override
    public void initializeProgressCallback() {
        final int duration = videoView.getDuration();
        if (playbackInfoListener != null) {
            playbackInfoListener.onDurationChanged(duration);
            playbackInfoListener.onPositionChanged(0);
        }
    }

    @Override
    public void seekTo(int position) {
        if (videoView != null) {
            videoView.seekTo(position);
        }
    }


    private void stopUpdatingCallbackWithPosition(boolean resetUIPlaybackPosition) {
            if (resetUIPlaybackPosition && playbackInfoListener != null) {
                playbackInfoListener.onPositionChanged(0);
            }
    }

    private void updateProgressCallbackTask() {
        if (videoView != null && videoView.isPlaying()) {
            int currentPosition = videoView.getCurrentPosition();

            if (playbackInfoListener != null) {
                playbackInfoListener.onPositionChanged(currentPosition);
            }
        }
    }
}
