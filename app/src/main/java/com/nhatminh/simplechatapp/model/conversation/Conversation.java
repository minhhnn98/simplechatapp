package com.nhatminh.simplechatapp.model.conversation;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.nhatminh.simplechatapp.model.message.UserMessage;
import com.nhatminh.simplechatapp.model.user.User;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "conversation")
public class Conversation {

    @NonNull
    @ColumnInfo(name = "conversation_id")
    @PrimaryKey
    String conversationId;

    @ColumnInfo(name = "member_ids")
    @TypeConverters(ConversationConverter.class)
    List<String> memberIds;

    public Conversation(String conversationId) {
        this();
        this.conversationId = conversationId;
    }

    public Conversation() {
        memberIds = new ArrayList<>();
    }

    public Conversation(String conversationId, List<String> members, List<UserMessage> messages) {
        this.conversationId = conversationId;
        this.memberIds = members;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public List<String> getMembers() {
        return memberIds;
    }

    public void setMembers(List<String> members) {
        this.memberIds = members;
    }
}
