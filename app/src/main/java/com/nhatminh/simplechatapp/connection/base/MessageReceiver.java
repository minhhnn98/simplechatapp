package com.nhatminh.simplechatapp.connection.base;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class MessageReceiver {

    protected Socket socket;

    protected DataInputStream socketInputStream = null;
    protected OutputStream fileOutputStream = null;

    protected OnMessageReceivedListener messageReceivedCallback;


    public MessageReceiver(Socket socket) {
        this.socket = socket;

        try {
            socketInputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void disconnect(){
        if(socket != null){
            try {
                socket.close();
                socketInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setOnMessageReceivedListener(OnMessageReceivedListener listener){
        this.messageReceivedCallback = listener;
    }




    public interface OnMessageReceivedListener{
        void onMessageReceived(String conversationId, String senderId, String message, long timestamp, String fileUrl);
    }



}
