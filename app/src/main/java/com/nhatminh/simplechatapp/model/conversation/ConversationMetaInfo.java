package com.nhatminh.simplechatapp.model.conversation;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "conversation_meta_info")
public class ConversationMetaInfo {
    @PrimaryKey(autoGenerate = true)
    private int metaInfoId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "last_sent_message")
    private String lastSentMessage;

    @ColumnInfo(name = "time_stamp")
    private long timeStamp;

    @ColumnInfo(name = "conversation_id")
    private String conversationId;

    public ConversationMetaInfo() {
    }

    public ConversationMetaInfo(int metaInfoId, String title, String lastMessageSent, long timeStamp, String conversationId) {
        this.metaInfoId = metaInfoId;
        this.title = title;
        this.lastSentMessage = lastMessageSent;
        this.timeStamp = timeStamp;
        this.conversationId = conversationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastSentMessage() {
        return lastSentMessage;
    }

    public void setLastSentMessage(String lastSentMessage) {
        this.lastSentMessage = lastSentMessage;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getMetaInfoId() {
        return metaInfoId;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public void setMetaInfoId(int metaInfoId) {
        this.metaInfoId = metaInfoId;
    }
}
