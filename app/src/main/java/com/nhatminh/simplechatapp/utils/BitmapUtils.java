package com.nhatminh.simplechatapp.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class BitmapUtils {

    public static Bitmap decodeBitmapWithRequestedSize(String filePath, int reqWidth, int reqHeight){
        // get bitmap size without loading it to memory
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // calculate sample size for scaling bitmap
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // decode with sample size
        options.inJustDecodeBounds = false;

        Bitmap bitmap = null;

        try{
            bitmap = BitmapFactory.decodeFile(filePath, options);
        }
        catch (OutOfMemoryError ex){
            return null;
        }

        return bitmap;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static byte[] bitmapToByteArray(Bitmap bitmap){

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        byte[] byteArrayOfBitmap = stream.toByteArray();

        return byteArrayOfBitmap;
    }

}
