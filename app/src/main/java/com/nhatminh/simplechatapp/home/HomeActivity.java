package com.nhatminh.simplechatapp.home;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nhatminh.simplechatapp.R;
import com.nhatminh.simplechatapp.chatlist.ChatListFragment;
import com.nhatminh.simplechatapp.database.ConversationDatabase;
import com.nhatminh.simplechatapp.friend.FriendListFragment;
import com.nhatminh.simplechatapp.model.conversation.ConversationMetaInfo;
import com.nhatminh.simplechatapp.connection.ServerConnection;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.nhatminh.simplechatapp.connection.ServerConnection.LOCAL_IMAGE_PATH;


public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView navigationView;

    Fragment chatListFragment;
    Fragment friendListFragment;
    Fragment accountFragment;

    FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);

        chatListFragment = new ChatListFragment();
        friendListFragment = new FriendListFragment();
        accountFragment = new FriendListFragment();

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        // default fragment
        loadFragment(chatListFragment);

        connectToServer();
        createFolders(LOCAL_IMAGE_PATH);
    }

    private void test(){
        ConversationMetaInfo metaInfo = new ConversationMetaInfo();
        metaInfo.setTitle("Android");
        metaInfo.setLastSentMessage("Hello");
        metaInfo.setTimeStamp(System.currentTimeMillis());

        List<String> ids = new ArrayList<>();
        ids.add("N2rocWYboMQycyVAu5PhEarYS5n2");
        ids.add("mnlJhvBohLNDWT2NiOp4NSp5VlG2");
        Collections.sort(ids);
        metaInfo.setConversationId(ids.get(0) + ids.get(1));

        ConversationDatabase database = ConversationDatabase.getInstance(this);
        database.getConversationMetaInfoDao().insertMetaInfo(metaInfo);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.navigation_message:
                loadFragment(chatListFragment);
                return true;

            case R.id.navigation_friends:
                loadFragment(friendListFragment);
                return true;

            case R.id.navigation_account:
                loadFragment(accountFragment);
                return true;
        }
        return false;
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    ServerConnection serverConnection;

    private void connectToServer(){
        serverConnection = ServerConnection.getInstance(currentUser.getUid(), getApplicationContext());
    }

    //todo: create folders for media message (if not exists)
    private boolean createFolders(String path){
        File folder = new File(path);
        if(!folder.exists()){
            return folder.mkdirs();
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        serverConnection.disconnect();
    }
}
