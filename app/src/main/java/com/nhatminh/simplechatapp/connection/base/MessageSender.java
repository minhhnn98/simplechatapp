package com.nhatminh.simplechatapp.connection.base;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MessageSender {
    protected Socket socket;
    protected String id;
    protected DataOutputStream socketOutputStream = null;

    public MessageSender() {
    }

    public MessageSender(Socket socket, String id) {
        this.socket = socket;
        this.id = id;

        try {
            socketOutputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected class IdAndTypeSenderRunnable implements Runnable{

        String type;

        public IdAndTypeSenderRunnable(String type) {
            this.type = type;
        }

        @Override
        public void run() {
            try {
                if(socketOutputStream != null){
                    socketOutputStream.writeUTF(id);
                    socketOutputStream.writeUTF(type);
                    socketOutputStream.flush();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public void disconnect(){
        if(socket != null){
            try {
                socket.close();
                socketOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
