package com.nhatminh.simplechatapp.connection.base;

import android.content.Context;

import com.nhatminh.simplechatapp.conversation.ConversationActivity;
import com.nhatminh.simplechatapp.database.ConversationDatabase;
import com.nhatminh.simplechatapp.model.conversation.ConversationMetaInfo;
import com.nhatminh.simplechatapp.model.message.UserMessage;

import java.io.IOException;
import java.net.Socket;

public class SocketConnection {

    protected Socket socket;
    protected MessageSender sender;
    protected MessageReceiver receiver;
    protected String id;
    protected Context appContext;

    public static ConversationActivity activity = null;

    public SocketConnection(String host, int port, String uId, Context context) throws IOException {
        socket = new Socket(host, port);
        this.id = uId;
        this.appContext = context.getApplicationContext();
    }

    protected void insertToDatabaseAsLastMessage(UserMessage message, String conversationId){
        ConversationMetaInfo metaInfo = ConversationDatabase.getInstance(appContext).getConversationMetaInfoDao().getConversationMetaInfo(conversationId);
        metaInfo.setTimeStamp(message.getCreatedAt());

        // text message
        if(message.getMessageContent() != null) {
            metaInfo.setLastSentMessage(message.getMessageContent());
        }
        // file message
        else {
            metaInfo.setLastSentMessage(message.getSenderId().substring(0, 4) + "... sent a file");
        }

        ConversationDatabase.getInstance(appContext).getConversationMetaInfoDao().updateMetaInfo(metaInfo);
    }

    protected void onNewMessageAdded(UserMessage userMessage){
        ConversationDatabase.getInstance(appContext).getUserMessageDao().insert(userMessage);
        insertToDatabaseAsLastMessage(userMessage, userMessage.getConversationId());

        if(activity != null){
            activity.insertToMessageList(userMessage);
        }
    }

    protected UserMessage createUserMessage(String content, String conversationId, String senderId, long timeStamp, String fileUrl){
        UserMessage message = new UserMessage();
        message.setConversationId(conversationId);
        message.setCreatedAt(timeStamp);
        message.setMessageContent(content);
        message.setSenderId(senderId);
        message.setFileUrl(fileUrl);

        return message;
    }

    public void disconnect(){
        if(sender!= null){
            sender.disconnect();
        }

        if(receiver != null){
            receiver.disconnect();
        }
    }

}
