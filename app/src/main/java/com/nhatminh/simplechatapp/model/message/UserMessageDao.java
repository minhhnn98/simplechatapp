package com.nhatminh.simplechatapp.model.message;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.google.firebase.database.core.Repo;

import java.util.List;

@Dao
public interface UserMessageDao {
    @Insert
    void insert(UserMessage... message);

    @Update
    void update(UserMessage... message);

    @Delete
    void delete(UserMessage... message);

    @Query("SELECT * FROM user_message WHERE conversation_id=:conversationId")
    List<UserMessage> getAllMessageOfConversation(final String conversationId);

}
