package com.nhatminh.simplechatapp.connection;

import android.content.Context;
import android.os.Environment;

import com.nhatminh.simplechatapp.connection.base.SocketConnection;
import com.nhatminh.simplechatapp.connection.binary.BinarySocket;
import com.nhatminh.simplechatapp.connection.textsocket.TextSocket;
import com.nhatminh.simplechatapp.conversation.ConversationActivity;

import java.io.IOException;

public class ServerConnection {

    public static final int SERVER_PORT = 8000;
    public static final String HOST_ADDRESS = "10.0.2.2";

    public static final int BUFFER_SIZE = 8192;

    public static final String TEXT_MESSAGE_TYPE = "text";
    public static final String BINARY_MESSAGE_TYPE = "binary";

    public static final String IMAGE_FILE_TYPE = "image";
    public static final String VIDEO_FILE_TYPE = "video";


    public static final String ACTION_REQUEST_GET_FILE = "action_request_get_file";
    public static final String ACTION_REQUEST_NOT_GET_FILE = "action_request_not_get_file";
    public static final String ACTION_SEND_FILE = "action_send_file";
    public static final String ACTION_SEND_FILE_META_INFO = "action_send_file_meta_info";

    public static final String LOCAL_IMAGE_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/ChatApp/Images/";

    public static final String DEFAULT_IMAGE_TYPE = ".png";
    public static final String DEFAULT_VIDEO_TYPE = ".mp4";

    private static volatile ServerConnection serverConnectionInstance;

    public String id;
    private Context appContext;

    BinarySocket binarySocket;
    TextSocket textSocket;

    private ServerConnection(final String id, Context applicationContext) {
        this.id = id;
        appContext = applicationContext.getApplicationContext();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    binarySocket = new BinarySocket(HOST_ADDRESS, SERVER_PORT, id, appContext);
                    textSocket = new TextSocket(HOST_ADDRESS, SERVER_PORT, id, appContext);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    public void sendBinaryFileMetaInfo(String conversationId, String receiverId, String fileType, String filePath) throws IOException {
        binarySocket.sendBinaryFileMetaInfo(conversationId, receiverId, fileType, filePath);
    }

    public void sendTextMessage(String content, String conversationId, String receiverId) throws IOException {
        textSocket.sendTextMessage(content, conversationId, receiverId);
    }

    public static ServerConnection getInstance(String uId, Context context){
        if (serverConnectionInstance == null) {
            synchronized(ServerConnection.class) {
                if(serverConnectionInstance == null) {
                    serverConnectionInstance = new ServerConnection(uId, context);
                }
            }
        }
        return serverConnectionInstance;
    }

    public void setActivity(ConversationActivity activity){
        SocketConnection.activity = activity;
    }

    public void disconnect(){
        if(binarySocket != null){
            binarySocket.disconnect();
        }

        if(textSocket != null){
            textSocket.disconnect();
        }
    }
}
