package com.nhatminh.simplechatapp.connection.textsocket;

import com.nhatminh.simplechatapp.connection.base.MessageSender;

import java.io.IOException;
import java.net.Socket;

import static com.nhatminh.simplechatapp.connection.ServerConnection.TEXT_MESSAGE_TYPE;

public class TextSender extends MessageSender {

    public TextSender(Socket socket, String id) {
        super(socket, id);
        new Thread(new IdAndTypeSenderRunnable(TEXT_MESSAGE_TYPE)).start();
    }

    public void sendTextMessage(String content, String conversationId, String receiverId) throws IOException {
        if(socket == null || content == null || receiverId == null || socketOutputStream == null){
            return;
        }

        socketOutputStream.writeUTF(TEXT_MESSAGE_TYPE);
        socketOutputStream.flush();

        socketOutputStream.writeUTF(receiverId);
        socketOutputStream.writeUTF(conversationId);
        socketOutputStream.writeUTF(content);
        socketOutputStream.flush();
    }
}
