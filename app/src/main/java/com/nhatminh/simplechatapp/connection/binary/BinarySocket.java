package com.nhatminh.simplechatapp.connection.binary;

import android.content.Context;
import android.util.Log;
import android.util.LruCache;

import com.nhatminh.simplechatapp.connection.base.MessageReceiver;
import com.nhatminh.simplechatapp.connection.base.SocketConnection;
import com.nhatminh.simplechatapp.model.message.UserMessage;
import com.nhatminh.simplechatapp.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_REQUEST_GET_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_REQUEST_NOT_GET_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.IMAGE_FILE_TYPE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.VIDEO_FILE_TYPE;

public class BinarySocket extends SocketConnection {

    //todo: how about send file to multiple users?
    private LruCache<String, byte[]> fileCache;
    private HashMap<String, String> fileIdentifier;

    public BinarySocket(String host, int port, String uId, Context context) throws IOException {
        super(host, port, uId, context);
        sender = new BinarySender(socket, id);
        receiver = new BinaryReceiver(socket);

        initializeFileCache();

        fileIdentifier = new HashMap<>();

        setMessageCallback();
    }

    private void initializeFileCache(){
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/12th of the available memory for this memory cache.
        final int cacheSize = maxMemory/12;

        fileCache = new LruCache<String, byte[]>(cacheSize){
            @Override
            protected int sizeOf(String key, byte[] value) {
                return value.length / 1024;
            }
        };
    }

    private void setMessageCallback(){
        setSenderCallback();
        setReceiverCallback();
    }

    private void setSenderCallback(){
        ((BinarySender)sender).setOnFileSentListener(new BinarySender.OnFileSentListener() {

            @Override
            public void onFileSentSucceed(String conversationId, String hashValue, byte[] data, String dataType) {
                onFileSentCompleted(conversationId, hashValue, data, dataType);
            }
        });

        ((BinarySender)sender).setOnBitmapDecodedListener(new BinarySender.OnBitmapDecodedListener() {
            @Override
            public void onDecodeCompleted(String hashValue, String filePath, byte[] data) {
                fileIdentifier.put(filePath, hashValue);
                fileCache.put(hashValue, data);
            }
        });
    }

    private void onFileSentCompleted(String conversationId, String hashValue, byte[] data, String dataType){
        String localPath = copyFileToLocalStorageIfNecessary(hashValue, dataType, data);

        if(localPath != null){
            UserMessage userMessage = createUserMessage(null, conversationId, id, System.currentTimeMillis(), localPath);
            onNewMessageAdded(userMessage);
        }
    }

    private void setReceiverCallback(){
        receiver.setOnMessageReceivedListener(new MessageReceiver.OnMessageReceivedListener() {
            @Override
            public void onMessageReceived(String conversationId, String senderId, String message, long timestamp, String fileUrl) {
                onNewMessageAdded(createUserMessage(message, conversationId, senderId, timestamp, fileUrl));
            }
        });

        ((BinaryReceiver)receiver).setOnFileMetaInfoReceivedListener(new BinaryReceiver.OnFileReceivedListener() {
            @Override
            public void onFileMetaInfoReceived(String conversationId, String senderId, String hashValue, int checksum, String fileType) {
                try {
                    String filePath = FileUtils.createMediaFilePathFromHashValue(hashValue, fileType);

                    if(FileUtils.isFileExisted(filePath)){
                        onNewMessageAdded(createUserMessage(null, conversationId, senderId, System.currentTimeMillis(), filePath));
                        ((BinarySender) sender).sendFileRequested(conversationId, senderId, id, hashValue, fileType, ACTION_REQUEST_NOT_GET_FILE);
                    }
                    else {
                        ((BinarySender) sender).sendFileRequested(conversationId, senderId, id, hashValue, fileType, ACTION_REQUEST_GET_FILE);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFileRequested(boolean isRequested, String conversationId, String receiverId, String hashValue, String fileType) {
                byte[] data = fileCache.get(hashValue);

                if(isRequested){
                    if(data != null){
                        try {
                            ((BinarySender) sender).sendBinaryFile(conversationId, receiverId, fileType, data, hashValue);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } //todo: else? (data not cached)
                }

                else {
                    if (data != null) {
                        onFileSentCompleted(conversationId, hashValue, data, fileType);
                    }
                }
            }
        });
    }

    private String copyFileToLocalStorageIfNecessary(String hashValue, String fileType, byte[] data){
        String filePath = FileUtils.createMediaFilePathFromHashValue(hashValue, fileType);

        File file = new File(filePath);

        if(!file.exists()){
            try {
                FileUtils.copyFile(data, filePath);
            } catch (IOException e) {
                e.printStackTrace();

                return null;
            }
        }

        return filePath;
    }

    public void sendBinaryFileMetaInfo(String conversationId, String receiverId, String fileType, String filePath) throws IOException {
        synchronized (sender) {
            switch (fileType) {
                case IMAGE_FILE_TYPE:
                    handleSentImage(conversationId, receiverId, fileType, filePath);
                    break;

                case VIDEO_FILE_TYPE:
                    handleSentVideo(conversationId, receiverId, filePath);
                    break;
            }

        }
    }

    private void handleSentImage(String conversationId, String receiverId, String fileType, String filePath) throws IOException {

        String hashValue = fileIdentifier.get(filePath);
        byte[] data = null;

        if(hashValue != null){
            data = fileCache.get(hashValue);
        }

        if(data == null) {
            ((BinarySender) sender).decodeAndSendImageMetaInfo(conversationId, receiverId, filePath);
        }
        else {
            ((BinarySender) sender).sendFileMetaInfo(conversationId, receiverId, hashValue, IMAGE_FILE_TYPE, data);
        }
    }

    private void handleSentVideo(String conversationId, String receiverId, String filePath) throws IOException {
        ((BinarySender) sender).sendVideoMetaInfo(conversationId, receiverId, filePath);
    }

    @Override
    public void disconnect() {
        super.disconnect();
        clearCache();
    }

    private void clearCache(){
        if(fileCache != null){
            fileCache.evictAll();
        }
    }

    //todo: copy image to local storage
    private void copyImageToLocalStorage(String oldPath){
        Log.e("Test","test");
    }
}
