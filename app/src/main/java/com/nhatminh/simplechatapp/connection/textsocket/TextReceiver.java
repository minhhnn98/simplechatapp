package com.nhatminh.simplechatapp.connection.textsocket;

import com.nhatminh.simplechatapp.connection.base.MessageReceiver;

import java.io.IOException;
import java.net.Socket;

public class TextReceiver extends MessageReceiver {
    public TextReceiver(Socket socket) {
        super(socket);

        new Thread(new TextReceiverRunnable()).start();
    }

    public class TextReceiverRunnable implements Runnable{

        @Override
        public void run() {

            while (true) {
                try {
                    String messageType = socketInputStream.readUTF();
                    String conversationId = socketInputStream.readUTF();
                    String senderId = socketInputStream.readUTF();
                    long timestamp = Long.valueOf(socketInputStream.readUTF());

                    String message = socketInputStream.readUTF();

                    if(messageReceivedCallback != null){
                        messageReceivedCallback.onMessageReceived(conversationId, senderId, message, timestamp, null);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
