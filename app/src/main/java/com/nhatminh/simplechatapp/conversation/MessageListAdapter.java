package com.nhatminh.simplechatapp.conversation;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhatminh.simplechatapp.R;
import com.nhatminh.simplechatapp.imageloader.ImageLoader;
import com.nhatminh.simplechatapp.model.message.UserMessage;
import com.nhatminh.simplechatapp.utils.DateManipulation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_TEXT_SENT = 1;
    private static final int VIEW_TYPE_TEXT_RECEIVED = 2;

    private static final int VIEW_TYPE_IMAGE_SENT = 3;
    private static final int VIEW_TYPE_IMAGE_RECEIVED = 4;

    private static final int DEFAULT_IMAGE_HEIGHT = 120;
    private static final int DEFAULT_IMAGE_WIDTH = 120;

    private static final int MAX_THUMBNAIL_ELEMENT = 10;

    private List<UserMessage> mMessageList;
    private String mCurrentUserId;
    HashMap<String, String> mMembersIdAndName;

    private RecyclerView mRecyclerView;

    private Context mContext;
    private ImageLoader mImageLoader;

    Bitmap defaultImage;

    List<Pair<Integer, Bitmap>> mImageList;

    public MessageListAdapter(Context context, List<UserMessage> messageList, String currentUserId, HashMap<String, String> membersIdAndName) {
        mMessageList = messageList;
        mCurrentUserId = currentUserId;
        mMembersIdAndName = membersIdAndName;

        mImageLoader = new ImageLoader(context);
        mContext = context;

        defaultImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_image);

        mImageList = new LinkedList<>();
        mImageLoader.setOnBitmapLoadingCompletedListener(new ImageLoader.OnBitmapLoadingCompletedListener() {
            @Override
            public void onBitmapLoadingCompleted(final Bitmap bitmap, final int position) {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (!isThumbnailExistInList(position)) {
                            addDecodedThumbnailToList(position, bitmap);
                        }

                        RecyclerView.ViewHolder holder = mRecyclerView.findViewHolderForAdapterPosition(position);

                        updateImageMessage(holder, bitmap);

                    }
                });
            }
        });
    }

    private void addDecodedThumbnailToList(int position, Bitmap bitmap) {
        if (mImageList.size() >= MAX_THUMBNAIL_ELEMENT) {
            mImageList.remove(0);
        }

        mImageList.add(new Pair<Integer, Bitmap>(position, bitmap));
    }

    private boolean isThumbnailExistInList(int pos) {
        for (int i = 0; i < mImageList.size(); i++) {
            if (mImageList.get(i).first == pos) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mRecyclerView = recyclerView;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        int position = holder.getAdapterPosition();

        Bitmap bitmap = getImageIfAvailable(position);
        if (bitmap != null) {
            updateImageMessage(holder, bitmap);
        }
    }

    public void updateImageMessage(RecyclerView.ViewHolder holder, Bitmap bitmap){
        if (holder != null && bitmap != null) {
            if(holder instanceof SentImageHolder){
                ((SentImageHolder) holder).messageImage.setImageBitmap(bitmap);

            } else if (holder instanceof ReceivedImageHolder){
                ((ReceivedImageHolder) holder).messageImage.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);


        updateImageMessage(holder, defaultImage);
    }

    private Bitmap getImageIfAvailable(int position) {
        for (int i = 0; i < mImageList.size(); i++) {
            if (mImageList.get(i).first == position) {
                return mImageList.get(i).second;
            }
        }
        return null;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType){
            case VIEW_TYPE_TEXT_SENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_text_sent, parent, false);
                return new SentTextHolder(view);

            case VIEW_TYPE_TEXT_RECEIVED:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_text_received, parent, false);
                return new ReceivedTextHolder(view);

            case VIEW_TYPE_IMAGE_SENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_image_sent, parent, false);
                return new SentImageHolder(view);

            case VIEW_TYPE_IMAGE_RECEIVED:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_image_received, parent, false);
                return new ReceivedImageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UserMessage message = (UserMessage) mMessageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_TEXT_SENT:
                ((SentTextHolder) holder).bind(message, position);
                break;

            case VIEW_TYPE_TEXT_RECEIVED:
                ((ReceivedTextHolder) holder).bind(message, position);
                break;

            case VIEW_TYPE_IMAGE_SENT:
                ((SentImageHolder) holder).bind(message, position);
                break;

            case VIEW_TYPE_IMAGE_RECEIVED:
                ((ReceivedImageHolder) holder).bind(message, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        UserMessage message = mMessageList.get(position);
        if(message.getSenderId().equals(mCurrentUserId)) {

            if(message.getMessageContent() != null){
                return VIEW_TYPE_TEXT_SENT;
            }
            else {
                return VIEW_TYPE_IMAGE_SENT;
            }
        }
        else {

            if(message.getMessageContent() != null){
                return VIEW_TYPE_TEXT_RECEIVED;
            }
            else {
                return VIEW_TYPE_IMAGE_RECEIVED;
            }
        }
    }

    public void clean(){
        mImageLoader.clean();
        recycleImageList();

        if(defaultImage != null){
            defaultImage.recycle();
            defaultImage = null;
        }
    }

    private void recycleImageList() {
        if (mImageList != null) {
            for (int i = 0; i < mImageList.size(); i++) {
                if (mImageList.get(i).second != null) {
                    mImageList.get(i).second.recycle();
                }
            }
            mImageList = null;
        }
    }

    private class SentTextHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        SentTextHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
        }

        void bind(UserMessage message, int position) {
            messageText.setText(message.getMessageContent());
            timeText.setText(DateManipulation.longTimeToString(message.getCreatedAt()) + " pos: " + position);
        }
    }

    private class ReceivedTextHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage;

        ReceivedTextHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            nameText = itemView.findViewById(R.id.text_message_name);
            profileImage = itemView.findViewById(R.id.image_message_profile);
        }

        void bind(UserMessage message, int position) {
            messageText.setText(message.getMessageContent());

            timeText.setText(DateManipulation.longTimeToString(message.getCreatedAt()) + " pos: " + position);
            nameText.setText(mMembersIdAndName.get(message.getSenderId()));

            // Insert the profile image from the URL into the ImageView.
            /**
             * Handle profile picture later
             *    profileImage.setImageResource(R.drawable.ic_launcher_background);
             */

        }
    }

    private class SentImageHolder extends RecyclerView.ViewHolder{
        TextView timeText;
        ImageView messageImage;
        public SentImageHolder(@NonNull View itemView) {
            super(itemView);

            messageImage = itemView.findViewById(R.id.image_message);
            timeText =  itemView.findViewById(R.id.text_message_time);
        }

        void bind(UserMessage message, int position) {
            /*Glide.with(messageImage.getContext())
                    .load(message.getFileUrl())
                    .into(messageImage);*/

            timeText.setText(DateManipulation.longTimeToString(message.getCreatedAt()) + " pos: " + position);

            mImageLoader.requestImage(message.getFileUrl(), position, DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
        }
    }

    private class ReceivedImageHolder extends RecyclerView.ViewHolder {
        TextView timeText, nameText;
        ImageView profileImage, messageImage;

        ReceivedImageHolder(View itemView) {
            super(itemView);

            messageImage = itemView.findViewById(R.id.image_message);
            timeText = itemView.findViewById(R.id.text_message_time);
            nameText = itemView.findViewById(R.id.text_message_name);
            profileImage = itemView.findViewById(R.id.image_message_profile);

        }

        void bind(UserMessage message, int position) {
            /*Glide.with(messageImage.getContext())
                    .load(message.getFileUrl())
                    .into(messageImage);*/

            timeText.setText(DateManipulation.longTimeToString(message.getCreatedAt()) + " pos: " + position);
            nameText.setText(mMembersIdAndName.get(message.getSenderId()));

            mImageLoader.requestImage(message.getFileUrl(), position, DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);

            // Insert the profile image from the URL into the ImageView.
            /**
             * Handle profile picture later
             *    profileImage.setImageResource(R.drawable.ic_launcher_background);
             */
        }
    }
}
