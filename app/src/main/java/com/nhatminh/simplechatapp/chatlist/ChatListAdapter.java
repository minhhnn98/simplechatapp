package com.nhatminh.simplechatapp.chatlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.google.firebase.auth.FirebaseAuth;
import com.nhatminh.simplechatapp.R;
import com.nhatminh.simplechatapp.model.conversation.ConversationMetaInfo;
import com.nhatminh.simplechatapp.utils.DateManipulation;

import java.util.List;

public class ChatListAdapter extends Adapter<ChatListAdapter.ChatListViewHolder> {

    private List<ConversationMetaInfo> conversationMetaInfoList;

    private ChatListClickListener listener;

    public ChatListAdapter(List<ConversationMetaInfo> conversationMetaInfoList, ChatListClickListener chatListClickListener) {
        this.conversationMetaInfoList = conversationMetaInfoList;
        this.listener = chatListClickListener;
    }

    public ChatListAdapter() {
    }

    @NonNull
    @Override
    public ChatListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_list, parent, false);
        return new ChatListViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return conversationMetaInfoList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ChatListViewHolder holder, int position) {
        ((ChatListViewHolder) holder).bind(conversationMetaInfoList.get(position), listener);
    }

    public List<ConversationMetaInfo> getConversationMetaInfoList() {
        return conversationMetaInfoList;
    }

    public void setConversationMetaInfoList(List<ConversationMetaInfo> conversationMetaInfoList) {
        this.conversationMetaInfoList = conversationMetaInfoList;
    }

    public ChatListClickListener getListener() {
        return listener;
    }

    public void setListener(ChatListClickListener listener) {
        this.listener = listener;
    }

    class ChatListViewHolder extends RecyclerView.ViewHolder{
        ImageView ivProfilePicture;
        TextView tvConversationName, tvLastMessageSent, tvTimeStamp;
        ChatListClickListener listener;


        public ChatListViewHolder(@NonNull View itemView) {
            super(itemView);
            ivProfilePicture = itemView.findViewById(R.id.ivUserProfilePicture);
            tvConversationName = itemView.findViewById(R.id.tvConversationName);
            tvLastMessageSent = itemView.findViewById(R.id.tvLastMessageSent);
            tvTimeStamp = itemView.findViewById(R.id.tvTimeStamp);
        }

        void bind(final ConversationMetaInfo conversationMetaInfo, final ChatListClickListener listener){
            /**
             * ivProfilePicture handle later
             */
            tvConversationName.setText(conversationMetaInfo.getTitle());
            tvLastMessageSent.setText(conversationMetaInfo.getLastSentMessage());
            tvTimeStamp.setText(DateManipulation.longTimeToString(conversationMetaInfo.getTimeStamp()));

            FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(getAdapterPosition(),conversationMetaInfo);
                }
            });
        }
    }
}
