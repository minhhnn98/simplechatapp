package com.nhatminh.simplechatapp.model.message;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.nhatminh.simplechatapp.model.conversation.Conversation;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Conversation.class,
                                    parentColumns = "conversation_id",
                                    childColumns = "conversation_id",
                                    onDelete = CASCADE),
        tableName = "user_message")
public class UserMessage {

    @ColumnInfo(name = "message_id")
    @PrimaryKey(autoGenerate = true)
    private int messageId;

    @ColumnInfo(name = "message_content")
    private String messageContent;

    @ColumnInfo(name = "sender_id")
    private String senderId;

    @ColumnInfo(name = "created_at")
    private long createdAt;

    @ColumnInfo(name = "conversation_id")
    private String conversationId;

    @ColumnInfo(name = "file_url")
    private String fileUrl;


    public UserMessage(String messageContent, String senderId, long createdAt) {
        this.messageContent = messageContent;
        this.senderId = senderId;
        this.createdAt = createdAt;
    }


    public UserMessage() {
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
