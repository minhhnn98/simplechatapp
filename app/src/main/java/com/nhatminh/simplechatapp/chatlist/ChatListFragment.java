package com.nhatminh.simplechatapp.chatlist;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nhatminh.simplechatapp.R;
import com.nhatminh.simplechatapp.conversation.ConversationActivity;
import com.nhatminh.simplechatapp.database.ConversationDatabase;
import com.nhatminh.simplechatapp.login.LoginActivity;
import com.nhatminh.simplechatapp.model.conversation.Conversation;
import com.nhatminh.simplechatapp.model.conversation.ConversationMetaInfo;
import com.nhatminh.simplechatapp.utils.GlobalConstants;

import java.util.List;


public class ChatListFragment extends Fragment implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    Button btnSignOut;
    RecyclerView rvChatList;
    ProgressBar pbLoadingConversation;

    List<ConversationMetaInfo> metaInfoList;
    ChatListAdapter adapter;

    Thread loadConversationThread;

    public ChatListFragment() {
    }

    public static ChatListFragment newInstance() {

        Bundle args = new Bundle();

        ChatListFragment fragment = new ChatListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView(view);

        loadConversationInfo();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setupView(View view){
        btnSignOut = view.findViewById(R.id.btnSignOut);
        btnSignOut.setOnClickListener(this);

        rvChatList = view.findViewById(R.id.rvChatList);
        rvChatList.setLayoutManager(new LinearLayoutManager(getActivity()));

        pbLoadingConversation = view.findViewById(R.id.pbLoadConversationProgress);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

       createAdapter();
    }

    private void createAdapter(){
        adapter = new ChatListAdapter();
        adapter.setListener(new ChatListClickListener() {
            @Override
            public void onClick(int position, ConversationMetaInfo conversationMetaInfo) {
                Intent intent = new Intent(getActivity(), ConversationActivity.class);
                intent.putExtra(GlobalConstants.CONVERSATION_ID, conversationMetaInfo.getConversationId());
                intent.putExtra(GlobalConstants.CONVERSATION_TITLE,conversationMetaInfo.getTitle());
                startActivity(intent);
            }
        });
    }

    private void loadConversationInfo(){
        loadConversationThread = new Thread(new Runnable() {
            @Override
            public void run() {
                metaInfoList = ConversationDatabase.getInstance(getContext()).getConversationMetaInfoDao().getLastNConversations(10);

                if(!loadConversationThread.isInterrupted()) {
                    adapter.setConversationMetaInfoList(metaInfoList);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            rvChatList.setAdapter(adapter);
                            pbLoadingConversation.setVisibility(View.GONE);
                        }
                    });

                }


            }
        });

        pbLoadingConversation.setVisibility(View.VISIBLE);
        loadConversationThread.start();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if(loadConversationThread!= null) {
            loadConversationThread.interrupt();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignOut:
                signOut();
                break;
        }
    }

    private void signOut() {
        firebaseAuth.signOut();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}
