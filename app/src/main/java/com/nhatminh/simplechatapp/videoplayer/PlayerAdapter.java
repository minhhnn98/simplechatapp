package com.nhatminh.simplechatapp.videoplayer;

public interface PlayerAdapter {

    void loadMedia(int resourceId);

    void loadMedia(String filePath);

    void release();

    boolean isPlaying();

    void play();

    void reset();

    void pause();

    void initializeProgressCallback();

    void seekTo(int position);
}
