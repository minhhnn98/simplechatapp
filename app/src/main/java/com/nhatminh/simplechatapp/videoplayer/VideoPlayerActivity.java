package com.nhatminh.simplechatapp.videoplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.nhatminh.simplechatapp.R;

import static com.nhatminh.simplechatapp.conversation.ConversationActivity.FILE_PATH;

public class VideoPlayerActivity extends AppCompatActivity {

    public static final String TAG = "MediaPlayerActivity";
    public static final int MEDIA_RES_ID = R.raw.all_falls_down;


    private PlayerAdapter playerAdapter;
    private boolean userIsSeeking = false;

    private SeekBar sbVideo;
    VideoView videoView;
    Button btPlay, btPause, btReset;

    String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        filePath = getIntent().getExtras().getString(FILE_PATH);

        initializeView();
        initializeSeekbar();
        initializePlaybackController();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        playerAdapter.loadMedia(filePath);
        Log.d(TAG, "onStart: create MediaPlayer");
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isChangingConfigurations() && playerAdapter.isPlaying()) {
        }

        else {
            playerAdapter.release();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private void initializeSeekbar() {
        sbVideo.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int userSelectedPosition = 0;

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        userIsSeeking = true;
                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            userSelectedPosition = progress;
                        }
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        userIsSeeking = false;
                        playerAdapter.seekTo(userSelectedPosition);
                    }
                });
    }

    private void initializePlaybackController() {
        VideoPlayerHolder videoPlayerHolder = new VideoPlayerHolder(this, videoView);
        videoPlayerHolder.setPlaybackInfoListener(new PlaybackListener());
        playerAdapter = videoPlayerHolder;
    }

    private void initializeView() {
        btPlay = findViewById(R.id.btPlay);
        btPause = findViewById(R.id.btPause);
        btReset = findViewById(R.id.btReset);

        videoView = findViewById(R.id.videoView);

        sbVideo = findViewById(R.id.sbVideo);

        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerAdapter.play();
            }
        });

        btPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerAdapter.pause();
            }
        });

        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerAdapter.reset();
            }
        });
    }

    public class PlaybackListener extends PlaybackInfoListener {

        @Override
        public void onDurationChanged(int duration) {
           sbVideo.setMax(duration);
        }

        @Override
        public void onPositionChanged(int position) {
            if (!userIsSeeking) {
                sbVideo.setProgress(position, true);
            }
        }

        @Override
        public void onStateChanged(State state) {
        }

        @Override
        public void onPlaybackCompleted() {
        }

        @Override
        public void onLogUpdated(String message) {
        }
    }
}
