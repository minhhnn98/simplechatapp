package com.nhatminh.simplechatapp.connection.binary;

import com.nhatminh.simplechatapp.connection.base.MessageReceiver;
import com.nhatminh.simplechatapp.utils.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_REQUEST_GET_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_REQUEST_NOT_GET_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_SEND_FILE;
import static com.nhatminh.simplechatapp.connection.ServerConnection.ACTION_SEND_FILE_META_INFO;

public class BinaryReceiver extends MessageReceiver {

    protected OnFileReceivedListener fileReceivedCallback;

    public BinaryReceiver(Socket socket) {
        super(socket);

        new Thread(new BinaryReceiverRunnable()).start();
    }

    public class BinaryReceiverRunnable implements Runnable{

        @Override
        public void run() {

            while (true) {
                try {
                    String action = socketInputStream.readUTF();
                    handleAction(action);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleAction(String action) throws IOException {
        switch (action){

            case ACTION_SEND_FILE:
                readIncomingFile();
                break;

            case ACTION_SEND_FILE_META_INFO:
                readFileMetaInfo();
                break;

            case ACTION_REQUEST_GET_FILE:
                readRequestGetFile();
                break;

            case ACTION_REQUEST_NOT_GET_FILE:
                readRequestNotGetFile();
                break;
        }
    }

    private void readRequestGetFile() throws IOException {
        String conversationId = socketInputStream.readUTF();
        String receiverId = socketInputStream.readUTF();
        String hashValue = socketInputStream.readUTF();
        String fileType = socketInputStream.readUTF();

        fileReceivedCallback.onFileRequested(true, conversationId, receiverId, hashValue, fileType);
    }

    private void readRequestNotGetFile() throws IOException {
        String conversationId = socketInputStream.readUTF();
        String receiverId = socketInputStream.readUTF();
        String hashValue = socketInputStream.readUTF();
        String fileType = socketInputStream.readUTF();

        fileReceivedCallback.onFileRequested(false, conversationId, receiverId, hashValue, fileType);
    }

    private void readFileMetaInfo() throws IOException {
        String conversationId = socketInputStream.readUTF();
        String senderId = socketInputStream.readUTF();
        String fileType = socketInputStream.readUTF();

        String hashValue = socketInputStream.readUTF();
        int checksum = socketInputStream.readInt();

        fileReceivedCallback.onFileMetaInfoReceived(conversationId, senderId,
                hashValue, checksum, fileType);
    }

    private void readIncomingFile() throws IOException {
        String conversationId = socketInputStream.readUTF();
        String senderId = socketInputStream.readUTF();
        String hashValue = socketInputStream.readUTF();
        String fileType = socketInputStream.readUTF();
        long timestamp = Long.valueOf(socketInputStream.readUTF());

        byte[] imageBytes;

        String filePath = FileUtils.createMediaFilePathFromHashValue(hashValue, fileType);

        File file = new File(filePath);
        fileOutputStream = new FileOutputStream(file);

        int checkSum = socketInputStream.readInt();
        int numbersOfChunk = socketInputStream.readInt();

        ArrayList<byte[]> chunks = new ArrayList<>();

        for (int i = 0; i < numbersOfChunk; i++) {
            int length = socketInputStream.readInt();
            imageBytes = new byte[length];

            // read from socket
            socketInputStream.readFully(imageBytes, 0, length);
            chunks.add(imageBytes);

            // write to local file
            fileOutputStream.write(imageBytes, 0, length);
        }

        if (fileOutputStream != null) {
            fileOutputStream.close();
        }

        if (messageReceivedCallback != null) {
            messageReceivedCallback.onMessageReceived(conversationId, senderId, null, timestamp, filePath);
        }
    }

    public void setOnFileMetaInfoReceivedListener(OnFileReceivedListener listener){
        this.fileReceivedCallback = listener;
    }

    public interface OnFileReceivedListener{
        void onFileMetaInfoReceived(String conversationId, String senderId, String hashValue, int checksum, String fileType);
        void onFileRequested(boolean isRequested, String conversationId, String receiverId, String hashValue, String fileType);
    }
}
