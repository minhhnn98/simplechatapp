package com.nhatminh.simplechatapp.model.conversation;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nhatminh.simplechatapp.model.message.UserMessage;

import java.lang.reflect.Type;
import java.util.List;

public class ConversationConverter {

    @TypeConverter
    public String memberIdListToJson(List<String> memberIds) {
        if (memberIds == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<String>>() {}.getType();
        String json = gson.toJson(memberIds, type);
        return json;
    }

    @TypeConverter
    public List<String> memberIdListJsonToString(String memberIdsJson) {
        if (memberIdsJson == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<String>>() {}.getType();
        List<String> memberIds = gson.fromJson(memberIdsJson, type);
        return memberIds;
    }
}
