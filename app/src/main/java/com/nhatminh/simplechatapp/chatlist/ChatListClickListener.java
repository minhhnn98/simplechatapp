package com.nhatminh.simplechatapp.chatlist;

import com.nhatminh.simplechatapp.model.conversation.ConversationMetaInfo;

public interface ChatListClickListener {
    void onClick(int position, ConversationMetaInfo conversationMetaInfo);
}
