package com.nhatminh.simplechatapp.database;

import android.content.Context;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.nhatminh.simplechatapp.model.conversation.Conversation;
import com.nhatminh.simplechatapp.model.conversation.ConversationDao;
import com.nhatminh.simplechatapp.model.conversation.ConversationMetaInfo;
import com.nhatminh.simplechatapp.model.conversation.ConversationMetaInfoDao;
import com.nhatminh.simplechatapp.model.message.UserMessage;
import com.nhatminh.simplechatapp.model.message.UserMessageDao;
import com.nhatminh.simplechatapp.model.user.User;

@Database(entities = {ConversationMetaInfo.class, Conversation.class, UserMessage.class}, version = 1)
public abstract class ConversationDatabase extends RoomDatabase {

    private static final String DB_NAME = "conversation_database.db";
    private static volatile ConversationDatabase databaseInstance;

    public static ConversationDatabase getInstance(Context context) {
        if (databaseInstance == null) {
            synchronized(ConversationDatabase.class) {
                if(databaseInstance == null) {
                    databaseInstance = create(context);
                }
            }
        }
        return databaseInstance;
    }

    private static ConversationDatabase create(final Context context) {
        return Room.databaseBuilder(
                context.getApplicationContext(),
                ConversationDatabase.class,
                DB_NAME).allowMainThreadQueries().build();
    }

    public abstract ConversationMetaInfoDao getConversationMetaInfoDao();
    public abstract ConversationDao getConversationDao();
    public abstract UserMessageDao getUserMessageDao();
}
