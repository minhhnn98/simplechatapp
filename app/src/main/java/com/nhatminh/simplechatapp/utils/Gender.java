package com.nhatminh.simplechatapp.utils;

public enum Gender {
    MALE,
    FEMALE,
    OTHER,
    NULL

}
